package com.example.demo.student;

import java.time.LocalDate;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StudentConfig {
	@Bean
	CommandLineRunner commandLineRunner(StudentRepository repository) {
		return args ->{
			Student maryam = new Student(
					"Maryam",
					"Mayam@gmail.com",
					LocalDate.of(2000, 6, 23)
					);
			Student alex = new Student(
					"Alex",
					"Alex@hotmail.com",
					LocalDate.of(1999, 4, 12)
					);
			repository.saveAll(
					List.of(maryam, alex)
					);
		};
		
	}

}
